FROM ubuntu:focal AS builder

# ------------------
# Install Packer
# ------------------
ARG PACKER_VERSION=1.6.1

RUN apt-get clean && apt-get update && apt-get install -y curl unzip && \
    curl -L -O https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip && \
    unzip packer_${PACKER_VERSION}_linux_amd64.zip && \
    mv packer /usr/bin && \
    rm packer_${PACKER_VERSION}_linux_amd64.zip && \
    packer -version

# ------------------
# Install Vault
# ------------------
ARG VAULT_VERSION=1.5.2

RUN curl -L -O https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_linux_amd64.zip && \
    unzip vault_${VAULT_VERSION}_linux_amd64.zip && \
    mv vault /usr/bin && \
    rm vault_${VAULT_VERSION}_linux_amd64.zip && \
    vault -version

# ------------------
# Install govc
# ------------------
ARG GOVC_VERSION=0.23.0

RUN curl -L -O https://github.com/vmware/govmomi/releases/download/v${GOVC_VERSION}/govc_linux_amd64.gz && \
    gunzip govc_linux_amd64.gz && \
    mv govc_linux_amd64 /usr/bin/govc && \
    chmod 755 /usr/bin/govc && \
    govc version

# ------------------
# Install Ansible
# ------------------

RUN apt-get install -y python3 python3-pip && \
    pip3 install ansible ansible-lint && \
    ansible-lint --version && \
    ansible --version

# ------------------
# Install vmWare dcli
# ------------------

RUN pip3 install dcli && \
    dcli --version

# ------------------
# Install AWS cli
# ------------------

RUN pip3 install awscli && \
    aws --version


FROM ubuntu:focal

COPY VMware-ovftool-4.4.0-16360108-lin.x86_64.bundle /tmp

# ovftool wants locales
RUN apt-get clean && apt-get update && apt-get install -y locales && locale-gen en_US.UTF-8

RUN /tmp/VMware-ovftool-4.4.0-16360108-lin.x86_64.bundle --console --required --eulas-agreed && \
    rm -f /tmp/VMware-ovftool-4.4.0-16360108-lin.x86_64.bundle

# ------------------
# Install Python
# ------------------

RUN apt-get install -y python3 python3-pkg-resources python3-distutils

# ------------------
# Install git, jq, ca-certificates
# ------------------

RUN apt-get install -y git jq ca-certificates

# ------------------
# Copy tools from builder
# ------------------

COPY --from=builder /usr/bin/packer /usr/bin
COPY --from=builder /usr/bin/vault /usr/bin
COPY --from=builder /usr/local/bin /usr/local/bin
COPY --from=builder /usr/local/lib/python3.8/dist-packages /usr/local/lib/python3.8/dist-packages
COPY --from=builder /usr/bin/govc /usr/bin

RUN packer -version && vault -version && ansible-lint --version && ansible --version && dcli --version && aws --version && govc version

ENTRYPOINT ["/usr/bin/packer"]
