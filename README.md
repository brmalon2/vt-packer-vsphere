VT NISSO Packer vSphere Docker Image
====================================

This project builds a Docker image with the tools required to build VT NISSO's base images in vSphere.

How to build:
```
docker build --build-arg ROLE_ID=<ROLE_ID> -t code.vt.edu:5005/brmalon2/vt-packer-vsphere .
```
